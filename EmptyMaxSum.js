var a, b, c;

        function isEmpty(obj)
        { 
            if(Object.keys(obj).length == 0)
            	return true;
            else
            	return false;
        }

        function objSum(obj) 
        {
	        var sum ;
            sum= 0;
	        for(var i in obj)
    		    sum += obj[i];
            return sum;
        }

        function objMax(obj) 
        {
	        var max;
	        max=obj[Object.keys(obj)[0]];
	        for (var i in obj)
  	            if (obj[i] > max) 
  	            	max = obj[i];
	        return max;
        }

        a = {};
        b = {
            me: 'you'
        };
        c = {
            John: 20,
            Mike: 40,
            Sam: 10
        };
         
        console.log(isEmpty(a)); 
        console.log(isEmpty(b)); 
        console.log(objSum(c)); 
        console.log(objMax(c));